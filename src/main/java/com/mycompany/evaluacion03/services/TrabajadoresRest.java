/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.evaluacion03.services;

import com.mycompany.evaluacion03.apis.dao.TrabajadorJpaController;
import com.mycompany.evaluacion03.apis.dao.exceptions.NonexistentEntityException;
import com.mycompany.evaluacion03.entity.Trabajador;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author jaguilar
 */
@Path("trabajadores")
public class TrabajadoresRest {
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarTrabajadores() {
        
        TrabajadorJpaController dao = new TrabajadorJpaController();
        List<Trabajador> trabajadores = dao.findTrabajadorEntities();
        return Response.ok(200).entity(trabajadores).build();
        
    }
    
    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response agregarTrabajador(Trabajador trabajador) {
        
        TrabajadorJpaController dao = new TrabajadorJpaController();
        try {
            dao.create(trabajador);
        } catch (Exception ex) {
            Logger.getLogger(TrabajadoresRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.ok(200).entity(trabajador).build();
        
    }
    
    
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response actualizarTrabajador(Trabajador trabajador) {
        
        TrabajadorJpaController dao = new TrabajadorJpaController();
        try {
            dao.edit(trabajador);
        } catch (Exception ex) {
            Logger.getLogger(TrabajadoresRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.ok(200).entity(trabajador).build();
        
    }
    
    
    @DELETE
    @Path("/{ideliminar}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response eliminarTrabajador(@PathParam("ideliminar") String ideliminar) {
        
        TrabajadorJpaController dao = new TrabajadorJpaController();
        try {
            dao.destroy(ideliminar);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(TrabajadoresRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.ok("Trabajador eliminado").build();
        
    }
    
    
    @GET
    @Path("/{idconsultar}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response consultarTrabajadorPorID(@PathParam("idconsultar") String idconsultar){
        
        TrabajadorJpaController dao = new TrabajadorJpaController();
        Trabajador trabajador = dao.findTrabajador(idconsultar);
        return Response.ok(200).entity(trabajador).build();
        
    }
}
