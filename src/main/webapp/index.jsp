<%-- 
    Document   : index
    Created on : 24-jun-2021, 17:27:41
    Author     : voice
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="styles.css" type="text/css" rel="stylesheet">
        <title>Evaluacion 3 - API Rest</title>
    </head>
    <body>
        <h1>Endpoints API trabajadores - Evaluacion 3 - Jacob Aguilar</h1>
        <br>
        <h2> Listar trabajadores - GET - https://evaluacion03-apis.herokuapp.com/api/trabajadores</h2>
        <h2> Buscar trabajador por ID - GET - https://evaluacion03-apis.herokuapp.com/api/trabajadores/{id}</h2>
        <h2> Agregar trabajador - POST - https://evaluacion03-apis.herokuapp.com/api/trabajadores</h2>
        <h2> Modificar trabajador - PUT - https://evaluacion03-apis.herokuapp.com/api/trabajadores</h2>
        <h2> Eliminar trabajador - DELETE - https://evaluacion03-apis.herokuapp.com/api/trabajadores/{id}</h2>
    </body>
</html>
